# Papilio-ZPUino-SOC
ZPUino Soft Processor for the Papilio FPGA that is able to run Arduino sketches.

Download the latest bit file here:
    https://gitlab.com/Papilio-FPGA/Papilio-ZPUino-SOC/-/jobs/artifacts/master/raw/zpuino-2.1-PapilioPro-Vanilla.bit?job=synthesize
    
Download the latest bin file here:
    https://gitlab.com/Papilio-FPGA/Papilio-ZPUino-SOC/-/jobs/artifacts/master/raw/zpuino-2.1-PapilioPro-Vanilla.bin?job=synthesize